# *Yet Another F@#%ing* Router

![npm](https://img.shields.io/npm/v/yaf-router?label=version&style=flat)![npm type definitions](https://img.shields.io/npm/types/yaf-router?style=flat)![Snyk Vulnerabilities for npm package](https://img.shields.io/snyk/vulnerabilities/npm/yaf-router?style=flat)![npm](https://img.shields.io/npm/dm/yaf-router?style=flat)![last commit](https://badgen.net/gitlab/last-commit/sjsone/node-yaf-router)

A basic HTTP-Router with zero dependencies. Made for NodeJS-HTTP(s)-Server but the route-handling only depends on `request.url` and `request.method` so it should work with anything. The *Middlewares* require `response.write()`, `response.end()` and `response.writableEnded`.

## Getting started

```js
const NodeHttp = require('http');
const {Router} = require('yaf-router')

const router = new Router()

// GET /bestUser/foobar => "Hello bestUser, foobar"
router.addRoute('/{user}/{msg}', ({parameter, response}) => {
  response.write(`Hello ${parameter.user}, ${parameter.msg}`)
  response.end()
})

const server = NodeHttp.createServer((request, response) => {
  router.handle(request, response)
});
server.listen(8080);
```



## Documentation

[Routes](#Routes)

[Middlewares](#Middleware)

### Interfaces 

#### HandlerData

The _HandlerData_ stores the [Parameter](#Parameter), _Request_, _Response_ and almost always the _written_ data.

```typescript
interface HandlerData {
    parameter: {[key: string]: string},
    request: NodeHttp.IncomingMessage,
    response: NodeHttp.ServerResponse,
    written?: Array<any>
}
```

### Routes

A *Route* consists of a *Method* and *URL*:  `GET /hallo/User`. The *Method* is optional and defaults to `GET`. 

```js
router.addRoute('/me', ({parameter, response}) => {
	// ...
  response.end()
})
// is the same as 
router.addRoute('GET /me', ({parameter, response}) => {
	// ...
  response.end()
})
```


You can define multiple *Routes* for the same *Handler* by passing an Array of *Routes*

```js
router.addRoute(['/me', 'POST /you'], ({parameter, response}) => {
	// handles: GET /me and POST /you
  response.end()
})
```

#### Parameter

*Parameter* are part of the URL like `/search/{query}` . 

```js
// GET /search/foobar => "Searching: foobar"
router.addRoute('/search/{query}', ({parameter, response}) => {
  response.write(`Searching ${parameter.query}`)
  response.end()
})
```

Order and count of _Parameter_ do not matter. There may be multiple *Paramater* in a row.

```js
// GET /user/bestUser/foobar => "Hello bestUser, foobar"
router.addRoute('/user/{user}/{msg}', ({parameter, response}) => {
  response.write(`Hello ${parameter.user}, ${parameter.msg}`)
  response.end()
})
```

Wildcard-_Parameter_ are defined by the URI-Part `/*` and have to come last. Wildcard-_Parameter_ are accessed by the `*`-Key. Normal _Parameter_ are prioritized and matched first. 

```js
// GET /wildcard/BestUser/foo/bar => "BestUser is trying to access foo/bar"
router.addRoute('/wildcard/{user}/*', ({parameter, response}) => {
  response.write(`${parameter.user} is trying to access ${parameter['*']}`)
  response.end()
})
```

If there are multiple *Parameter* with the same key in a URL only the last value will be used. In the future this case may be handled as an `Array`. 

```js
// GET /search/foo/spacing/bar => "Searching: bar"
router.addRoute('/search/{search}/spacing/{search}', ({parameter, response}) => {
  response.write(`Searching: ${parameter.search}`)
  response.end()
})
```



### Middleware

A _Middleware_ implements the `MiddlewareInterface`. _Middlewares_ are called **after** routing so it is not possible to reroute via _Middlewares_.

```javascript
class CorsMiddleware implements MiddlewareInterface {
  getTypes() {
    return [
      MiddlewareOrderType.Header
    ]
  }
  
  getName() {
    return 'CorsMiddleware'
  }
  
  async handleByType(type: MiddlewareOrderType, handlerData: HandlerData, path: string = '') {
    if(type !== MiddlewareOrderType.Header) {
      return
    }
    handlerData.response.setHeader('Access-Control-Allow-Origin', '*');
		handlerData.response.setHeader('Access-Control-Request-Method', '*');
		handlerData.response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
		handlerData.response.setHeader('Access-Control-Allow-Headers', '*');
  }
}
```

The order of _Middleware_ execution and their suggested usage is:

1. **Security**: Makes security checks and ends the response if needed
2. **Header**: Only adds header to the response
3. **Data**: Enriches the _HandlerData_ with additional information
4. **Result**: Manipulates the _Response_
5. **Finisher**: Can be used for caching for example

Every _Handler_ can end the _Response_ but only expected should do so. 

#### Included Middlewares

##### Cache

A simple Cache implementation. Uses `MiddlewareOrderType.Data` if cache hit and `MiddlewareOrderType.Finisher` to fill cache. Default Cache-TTL is 60 seconds. 
Only caches **GET**, **OPTIONS** and **HEAD** methods. 

#### Cors

A simple Middleware that adds CORS-Header. 