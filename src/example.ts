import NodeHttp, { request } from 'http'
import { RequestOptions } from 'https'
import { Router, Middlewares, HandlerData } from './Router'


const router = new Router()

const port = process.env.PORT || 8801

const cache = new Middlewares.Cache()

const get = async (url: string) => new Promise((res, rej) => {
    NodeHttp.get(url, (resp) => {
        let data = '';
        resp.on('data', chunk => data += chunk);
        resp.on('end', () => res(data));
    }).on("error", (err) => rej(err));
})

const patch = async (url: string) => new Promise((res, rej) => {
    const uri = new URL(url)

    const requestOptions: RequestOptions = {
        method: 'POST',
        hostname: uri.hostname,
        port: uri.port,
        path: uri.pathname
    }
    console.log(requestOptions)
    NodeHttp.request(requestOptions, (resp) => {
        let data = '';
        resp.on('data', chunk => data += chunk);
        resp.on('end', () => res(data));
    }).on("error", (err) => rej(err));
})

router.addMiddleware(cache)

async function getHandler({ parameter, response, request }: HandlerData) {
    response.write(`get item ${parameter.id}`)
    response.end()
}
router.addRoute('GET /item/{id}', getHandler)

async function patchHandler({ parameter, response, request }: HandlerData) {
    response.write(`patch item ${parameter.id}`)
    response.end()
}
router.addRoute('PATCH /item/{id}', patchHandler)

const server = NodeHttp.createServer((request, response) => {
    router.handle(request, response)
});

server.listen(port);
console.log(`listening on ${port}`)

; (async () => {
    //console.log(await get('http://10.0.1.1:8801/item/1234'))
    // console.log(await patch('http://10.0.1.1:8801/item/1234'))
})()