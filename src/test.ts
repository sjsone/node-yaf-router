import { Router } from './Router'
import { IncomingMessage, ServerResponse } from 'http'
import { TestGroup } from 'yaf-test'

const router = new Router()

let lastResult: any = null
function setLastResult({ parameter }: any) { lastResult = parameter }

function handle(url: string, method: string = 'GET') {
    router.handle(<IncomingMessage>{
        url, method
    }, <ServerResponse>{})
    return lastResult
}

// TODO: create test for routes
router.setDefaultHandler(_ => { lastResult = 'DEFAULT_HANDLER' })
router.addRoute('/{user}/{blu}', setLastResult)
router.addRoute('/search/{search}', setLastResult)

router.addRoute(['/test', 'POST /test/*'], setLastResult)
router.addRoute('GET /item/{getId}', setLastResult)
router.addRoute('PATCH /item/{patchId}', setLastResult)

const exactMatch = TestGroup.Area('Exact match', test => {
    test('simple', handle('/test')).equal({})
    test('get id', handle('/item/123')).equal({ getId: '123' })
    test('path id', handle('/item/123', 'PATCH')).equal({ patchId: '123' })
    test('with rest', handle('/test/asdf/test', 'post')).equal({ "*": "asdf/test" })
    test('with urldecode', handle('/search/m%C3%BCnchen')).equal({ "search": "münchen" })
    test('with rest with urldecode', handle('/test/asdf/m%C3%BCnchen', 'post')).equal({ "*": "asdf/münchen" })
}, false)


const defaultHandler = TestGroup.Area('Default handler', test => {
    test('simple', handle('/asdf')).is('DEFAULT_HANDLER')
    test('with arguments', handle('//testa/tokenasdf/bla?asdf=asf')).is('DEFAULT_HANDLER')
}, false)

const variable = TestGroup.Area('Variable', test => {
    test('variable after variable', handle('/foo/bar')).equal({ "blu": "bar", "user": "foo" })

}, false)

TestGroup.GroupResultArea('Routes', [
    exactMatch,
    defaultHandler,
    variable
])