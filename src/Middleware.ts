import { HandlerData } from "./Router";

export enum MiddlewareOrderType {
    Security = 'security',
    Header = 'header',
    Data = 'data',
    Result = 'result',
    Finisher = 'finisher'
}

export interface MiddlewareInterface {
    getName: () => string
    getTypes: () => MiddlewareOrderType[]

    handleByType: (type: MiddlewareOrderType, handlerData: HandlerData, path?: string) => Promise<any>
}