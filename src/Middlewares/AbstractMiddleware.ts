import { MiddlewareInterface, MiddlewareOrderType } from "../Middleware";
import { HandlerData } from "../Router";

export abstract class AbstractMiddleware implements MiddlewareInterface {
    protected abstract name: string
    protected abstract types: MiddlewareOrderType[]

    getName() {
        return this.name
    }

    getTypes() {
        return this.types
    }

    canHandleType(type: MiddlewareOrderType) {
        return this.types.includes(type)
    }

    async handleByType(type: MiddlewareOrderType, handlerData: HandlerData, path?: string) {
        if(!this.canHandleType(type)) {
            return 
        }
        return this.handle(type, handlerData, path)
    }

    abstract handle(type: MiddlewareOrderType, handlerData: HandlerData, path?: string): Promise<any>
}