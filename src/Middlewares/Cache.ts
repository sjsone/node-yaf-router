import { MiddlewareInterface, MiddlewareOrderType } from "../Middleware";
import { HandlerData } from "../Router";


export interface CacheEntryInterface {
    date: Date
    data: any
    identifier: string
    ttl: number
}

export class Cache implements MiddlewareInterface {
    protected cacheData: { [key: string]: CacheEntryInterface } = {}

    getName() {
        return 'Cache'
    }

    getTypes() {
        return [
            MiddlewareOrderType.Data,
            MiddlewareOrderType.Finisher
        ]
    }

    /**
     * @param path matched Route path
     * @param handlerData 
     * @returns 
     */
    buildCacheIdentifier(path: string, handlerData: HandlerData) {
        const params = handlerData.parameter
        const paramsKeys = Object.keys(params).sort()
        return [
            path,
            ...paramsKeys.map(key => `${key}-${params[key]}`)
        ].join('_')
    }


    /**
     * Checks wether the path is cachable based on the used method
     * @param path 
     * @returns 
     */
    protected cachablePath(path: string) {
        return ['GET', 'OPTIONS', 'HEAD'].find(method => path.startsWith(method)) !== undefined
    }

    /**
     * @param handlerData 
     * @param path matched Route path
     * @returns 
     */
    async handleData(handlerData: HandlerData, path: string = '') {
        if(!this.cachablePath(path)) {
            return
        }

        const identifier = this.buildCacheIdentifier(path, handlerData)
        const entry = this.cacheData[identifier]

        if (entry) {
            const now = (new Date()).getTime()
            if (now > entry.date.getTime() + entry.ttl) {
                delete this.cacheData[identifier]
                return
            }

            for (const chunk of entry.data) {
                handlerData.response.write(chunk)
            }

            handlerData.response.end()
        }
    }

    /**
     * @param identifier Cache identifier
     * @param written written data
     * @param ttl Time to live. Defaults to 60 seconds
     * @returns Cache data
     */
    async buildCacheData(identifier: string, written: any[], ttl: number = 60*1000) {
        return {
            date: new Date,
            data: written,
            identifier,
            ttl // 60 seconds
        }
    }

    /**
     * @param handlerData 
     * @param path matched Route path
     */
    async handleFinisher(handlerData: HandlerData, path: string = '') {
        if(!this.cachablePath(path)) {
            return
        }
        const identifier = this.buildCacheIdentifier(path, handlerData)
        this.cacheData[identifier] = await this.buildCacheData(identifier, handlerData.written ?? [])
    }

    /**
     * @param type 
     * @param handlerData 
     * @param path matched Route path
     * @returns 
     */
    async handleByType(type: MiddlewareOrderType, handlerData: HandlerData, path: string = '') {
        if (type === MiddlewareOrderType.Data) {
            return this.handleData(handlerData, path)
        }
        if (type === MiddlewareOrderType.Finisher) {
            return this.handleFinisher(handlerData, path)
        }
    }
}