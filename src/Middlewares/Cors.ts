import { MiddlewareInterface, MiddlewareOrderType } from "../Middleware";
import { HandlerData } from "../Router";
import { AbstractMiddleware } from './AbstractMiddleware'

export class Cors extends AbstractMiddleware {
    protected name = 'CORS'
    protected types = [
        MiddlewareOrderType.Header
    ]

    async handle(type: MiddlewareOrderType, handlerData: HandlerData, path: string = '') {
        handlerData.response.setHeader('Access-Control-Allow-Origin', '*');
        handlerData.response.setHeader('Access-Control-Request-Method', '*');
        handlerData.response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
        handlerData.response.setHeader('Access-Control-Allow-Headers', '*');
    }
}