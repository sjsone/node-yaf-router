import * as NodeHttp from 'http'
import { MiddlewareInterface, MiddlewareOrderType } from './Middleware';

export * as Middlewares from './Middlewares/exports';


type BaseData = { [key: string]: any; }

export interface RoutePath {
    handler: { [key: string]: Function },
    routes: RoutePath | {}
    methods: Array<string>
    path: string
}

export interface HandlerData {
    parameter: { [key: string]: string },
    request: NodeHttp.IncomingMessage,
    response: NodeHttp.ServerResponse,
    written?: Array<any>
}

export interface RouteHandlerReturn {
    handler: Function,
    data?: BaseData,
    method: string,
    path?: string
}

const MiddlewareOrderBeforeHandler = [
    MiddlewareOrderType.Security,
    MiddlewareOrderType.Header,
    MiddlewareOrderType.Data
]

const MiddlewareOrderAfterHandler = [
    MiddlewareOrderType.Result,
    MiddlewareOrderType.Finisher
]

export class Router {

    protected serverRoutes: BaseData = {}
    protected defaultHandler = (data: HandlerData) => { data.response.statusCode = 404; data.response.end('') }
    protected middlewares: { [key: string]: MiddlewareInterface[] } = {
        [MiddlewareOrderType.Security]: [],
        [MiddlewareOrderType.Header]: [],
        [MiddlewareOrderType.Data]: [],
        [MiddlewareOrderType.Result]: [],
        [MiddlewareOrderType.Finisher]: [],
    }

    public createRecurseRoutePaths(method: string, parts: Array<string>, routes: any): RoutePath {
        const part = parts.length > 0 ? parts.splice(0, 1)[0] : '/'
        if (!routes[part]) {
            const routePath: RoutePath = { handler: {}, routes: {}, methods: [method], path: '' };
            routes[part] = routePath;
        } else {
            if (!(routes[part].methods.includes(method))) {
                routes[part].methods.push(method)
            }
        }

        if (parts.length > 0) {
            return this.createRecurseRoutePaths(method, parts, routes[part].routes)
        }
        return routes[part]
    }

    public getRecurseRouteHandler(method: string, parts: Array<string>, routes: any, data?: BaseData): RouteHandlerReturn {
        const DefaultReturn: RouteHandlerReturn = { handler: this.defaultHandler, data: data, method };
        let part = parts.length > 0 ? parts.splice(0, 1)[0] : '/'
        if (!part) {
            return DefaultReturn;
        }
        if(routes === undefined) {
            return DefaultReturn
        }
        const urlData: BaseData = data || {};
        if (!routes[part]) {
            for (let routeIndex in routes) {
                if (routeIndex.match(/\{[\w]*\}/gm)) {
                    const key = routeIndex.replace('{', '').replace('}', '')
                    urlData[key] = decodeURIComponent(part)
                    part = routeIndex
                    break;
                }
                if (routeIndex === '*') {
                    urlData[routeIndex] = decodeURIComponent([part, ...parts].join('/'))
                    part = routeIndex
                    break;
                }
            }
        }
        if (routes[part] && !(routes[part].methods.includes(method))) {
            return DefaultReturn
        }

        if (part === '*') {
            return { handler: routes[part].handler[method], data: urlData, method, path: `${method} ${routes[part].path}` }
        } else if (parts.length > 0) {
            return this.getRecurseRouteHandler(method, parts, routes[part].routes, urlData)
        } else if (routes[part] && routes[part].handler[method]) {
            return { handler: routes[part].handler[method], data: urlData, method, path: `${method} ${routes[part].path}` }
        } else {
            return DefaultReturn;
        }
    }

    public setDefaultHandler(handler: (data: HandlerData) => any) {
        this.defaultHandler = handler;
    }

    public addRoute(routes: string | Array<string>, handler: (data: HandlerData) => any) {
        if (!Array.isArray(routes)) {
            routes = [routes]
        }
        for (const route of routes) {
            const methodParts = route.split(' ').filter(e => !!e)
            let method = 'GET'
            let url = methodParts[0]
            if (methodParts.length > 1) {
                method = methodParts[0].toUpperCase()
                url = methodParts[1]
            }

            const parts = url.split('/').filter(Boolean)

            if(this.serverRoutes[method] === undefined) {
                this.serverRoutes[method] = {}
            }
        
            const path = this.createRecurseRoutePaths(method, parts, this.serverRoutes[method])
            path.handler[method] = handler
            path.path = url
        }
    }

    public addMiddleware(middleware: MiddlewareInterface) {
        for (const type of middleware.getTypes()) {
            if (!Object.values(MiddlewareOrderType).includes(type)) {
                throw new Error(`Unknown Middleware Type ${type} in ${middleware.getName()}`)
            }
            this.middlewares[type].push(middleware)
        }


    }

    protected createHandlerData(request: NodeHttp.IncomingMessage, response: NodeHttp.ServerResponse, data?: BaseData): HandlerData {
        return {
            parameter: data ?? {},
            request: request,
            response: response,
            written: Array()
        }
    }

    public async handle(req: NodeHttp.IncomingMessage, res: NodeHttp.ServerResponse) {
        const [path] = req.url!.split('?').filter(Boolean)
        const parts = path!.split('/').filter(Boolean)
        const method = '' + req.method?.toUpperCase()
        const { handler, data, path: routePath } = this.getRecurseRouteHandler(method, parts, this.serverRoutes[method])

        const handlerData = this.createHandlerData(req, res, data)

        const resWrite = res.write
        if(resWrite !== undefined) {
            res.write = (chunk: any, encoding: any, cb?: any): any => {
                handlerData.written?.push(chunk)
                return resWrite.apply(res, [chunk, encoding, cb])
            }
        }

        for (const type of MiddlewareOrderBeforeHandler) {
            for (const middleware of this.middlewares[type]) {
                await middleware.handleByType(type, handlerData, routePath)
                if (res.writableEnded) {
                    return
                }
            }
        }

        await handler(handlerData);

        for (const type of MiddlewareOrderAfterHandler) {
            for (const middleware of this.middlewares[type]) {
                await middleware.handleByType(type, handlerData, routePath)
                if (res.writableEnded) {
                    return
                }
            }
        }
    }
}
